# Contact

Un simple gestionnaire de contacts en ligne de commande.

Les données sont sauvegardées (par défaut) dans le dossier `$HOME/.config/contact`. Il est possible d'outrepasser ce choix en spécifiant la variable `$CONTACT_DIR`.

## Dépendances 
Ce script dépend de dialog ou Xdialog. Il utilisera par défaut dialog. Il peut utiliser Xdialog en substitut, mais l'utilisation est bien moins pratique.
Il dépend aussi de bash : on utilise la commande `local` qui n'est pas standard avec `sh` POSIX(d'après shellcheck).

## Licence 
Publié sous les termes de la [WTFPL](http://www.wtfpl.net/txt/copying/).

