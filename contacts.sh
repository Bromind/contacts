#!/bin/bash

# bash est requis car on utilise : `local`


get_config_folder() {
	if [ -z ${CONTACT_DIR+x} ]; then 
		CONTACT_DIR="$HOME/.config/contact"
	fi

	if [ ! -e "$CONTACT_DIR" ]; then
		echo "Création du répertoire dans le dossier $CONTACT_DIR."
		mkdir "$CONTACT_DIR"
	fi

	if [ ! -e "$CONTACT_DIR/contacts" ]; then
		mkdir "$CONTACT_DIR/contacts"
	fi

	if [ ! -e "$CONTACT_DIR/repertoire" ]; then
		touch "$CONTACT_DIR/repertoire"
	fi
}

get_dialog_prog() {
	for prog in dialog Xdialog ; do
		if command -v "$prog"
		then 
			export DIALOG="$prog"
			return
		fi
	done
	echo "Impossible de trouver dialog ou un programme alternatif" 2>&1
	exit 255
}

clean() {
	echo "Détruit tous les fichiers utilisés"
	rm -r "$CONTACT_DIR"
}

#1 Nom
#2 Prénom
#3 Mail
#4 Adresse 
add_contact() {
	local numero
	local fichier
	numero=$(wc -l "$CONTACT_DIR/repertoire" | cut -d ' ' -f 1)
	fichier="$CONTACT_DIR/contacts/$numero"
	echo "$numero $1 $2" >> "$CONTACT_DIR/repertoire"
	touch "$fichier"
	printf "Nom : %s\\nPrénom : %s\\nMail : %s\\nAdresse : %s" "$1" "$2" "$3" "$4">> "$fichier"
}

#1 Numéro d'identifiant du contact
show_contact() {
	local fichier
	fichier=$(head -n "$(dc -e "$1 1 + p")" "$CONTACT_DIR/repertoire" | tail -n 1 | cut -d ' ' -f 1)
	"$DIALOG" --title "Contact" --editbox "$CONTACT_DIR/contacts/$fichier" 0 0
}

add_cmd() {
	local nom
	local prenom
	local mail
	local adresse
	local tmp
	tmp="$(mktemp /tmp/contacts.XXX)"
	"$DIALOG" --title "Nouveau Contact" --form "Entrez les coordonnées du nouveau contact" 0 0 10 "Prénom" 1 1 "" 1 10 30 0 "Nom" 2 1 "" 2 10 30 0 "Mail" 3 1 "" 3 10 30 0 "Adresse" 4 1 "" 4 10 30 0 2> "$tmp" || \
		return
	prenom="$(head -n 1 "$tmp")"
	nom="$(head -n 2 "$tmp" | tail -n 1)"
	mail="$(head -n 3 "$tmp" | tail -n 1)"
	adresse="$(head -n 4 "$tmp" | tail -n 1)"

	rm "$tmp"

	if [ -z "$prenom" ] || [ -z "$nom" ]; then
		"$DIALOG" --title "Erreur" --msgbox "Impossible d'ajouter le contact car le nom et le prénom doivent être définis." 0 0
	else 
		add_contact "$nom" "$prenom" "$mail" "$adresse"
	fi
}

search_cmd() {
	local donnee
	local tmp
	local ligne
	tmp="$(mktemp /tmp/contacts.XXX)"
	dialog --title "Chercher un contact" --inputbox "Quel terme rechercher ?" 0 0 2> "$tmp"
	donnee="$(cat "$tmp")"
	echo "" > "$tmp"
	ligne="$(grep -h "$donnee" "$CONTACT_DIR/contacts/"*)"
	"$DIALOG" --title "Résultats de la recherche" --msgbox "$ligne" 0 0
	rm "$tmp"
}

show_cmd() {
	local numero
	local contacts
	local tmp
	tmp="$(mktemp /tmp/contacts.XXX)"
	contacts="$(awk -- "{printf \"%s \\\"%s %s\\\" \", \$1, \$2, \$3}" "$CONTACT_DIR"/repertoire)"
	echo "$contacts"
	eval "$DIALOG --no-tags --title \"Liste de contacts\" --menu \"Quel contact afficher ?\" 0 0 10 $contacts 2> $tmp" 
	numero="$(cat "$tmp")"
	rm "$tmp"
	show_contact "$numero"
}

read_exec_cmd() {
	local cmd
	local tmp
	tmp="$(mktemp /tmp/contacts.XXX)"
	"$DIALOG" --no-tags --title "Menu principal" --menu "Quelle action faire ?" 0 0 0 a "Ajouter un contact" c "Chercher un contact" d "Supprimer les fichiers locaux et quitter" m "Montrer un contact" q "Quitter" 2> "$tmp"
	cmd="$(cat "$tmp")"
	rm "$tmp"
	case "$cmd" in
		a) 	add_cmd
			return 0;;
		c)	search_cmd
			return 0;;
		d)	clean
			return 1;;
		m)	show_cmd
			return 0;;
		q) return 1;;
	esac
}

loop() {
	local fin
	fin="0"
	while [ $fin -eq 0 ]; 
	do
		read_exec_cmd
		fin="$?"
	done
}

tput smcup
get_dialog_prog
get_config_folder
loop
tput rmcup
